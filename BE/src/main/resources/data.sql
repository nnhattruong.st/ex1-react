DROP TABLE IF EXISTS news;
CREATE TABLE news (
id UUID  PRIMARY KEY,
name VARCHAR NOT NULL,
level INT NOT NULL
);

INSERT INTO news (id, name, level)
VALUES
('6a48d982-75ec-4dfc-bfb5-ae92b12f1427','Price of goods increase 4% on first 3 month of this year',	0),
('ebffc94e-8348-41e2-b4a9-e3b301d1a7ea','Crypto going down in every where',	1),
('ae32cb71-f93a-4e04-9a82-81f42c84ade3','VietNam airline wana sell 11 plane',	1),
('3a8a1a1a-caa6-4a07-9aea-28fafb766397','Number of people die by covid 19 is increase every hour',	2);