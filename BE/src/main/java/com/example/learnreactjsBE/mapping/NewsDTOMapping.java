package com.example.learnreactjsBE.mapping;

import com.example.learnreactjsBE.model.News;
import com.example.learnreactjsBE.model.dto.NewsDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class NewsDTOMapping {
    public List<NewsDTO> newsDTOSMapping(List<News> lstNew) {
        List<NewsDTO> newsDTOList = new ArrayList<>();
        lstNew.forEach(news -> newsDTOList.add(NewsDTO.builder().id(news.getId()).description(news.getName()).level(news.getLevel()).build()));
        return newsDTOList;
    }
}
