package com.example.learnreactjsBE.controller;

import com.example.learnreactjsBE.model.News;
import com.example.learnreactjsBE.model.dto.NewsDTO;
import com.example.learnreactjsBE.model.request.NewsRequest;
import com.example.learnreactjsBE.model.response.Body;
import com.example.learnreactjsBE.model.response.GeneralResponse;
import com.example.learnreactjsBE.model.response.NewsResponseBody;
import com.example.learnreactjsBE.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/news")
public class NewsController {

    @Autowired
    NewsService newsService;

    @GetMapping(value = "/get-all-news")
    public ResponseEntity<GeneralResponse> getAllNews(){
        List<NewsDTO> lstNewDTO = newsService.getAll();
        Body<List<NewsDTO>> lstNewDTORes = new Body<>(lstNewDTO);
        GeneralResponse newsResponse =  GeneralResponse.builder().hdr("Success").bd(lstNewDTORes).build();
        System.out.println(newsResponse.toString());
        return ResponseEntity.ok(newsResponse);
    }

    @PostMapping(value = "/insert-news")
    public ResponseEntity<GeneralResponse> insertNews(@RequestBody NewsRequest inputRequest){
        News newsStoreDB = newsService.insertNews(inputRequest);
        Body<News> newsBody = new Body<>(newsStoreDB);
        GeneralResponse newsResponse =  GeneralResponse.builder().hdr("Success").bd(newsBody).build();
        System.out.println(newsResponse.toString());
        return ResponseEntity.ok(newsResponse);
    }

    @PutMapping(value = "/update-news")
    public ResponseEntity<GeneralResponse> updateNews(@RequestBody NewsRequest inputRequest){
        News newsStoreDB = newsService.updateNews(inputRequest);
        Body<News> newsBody = new Body<>(newsStoreDB);
        GeneralResponse newsResponse =  GeneralResponse.builder().hdr("Success").bd(newsBody).build();
        System.out.println(newsResponse.toString());
        return ResponseEntity.ok(newsResponse);
    }

    @DeleteMapping(value = "/delete-news")
    public ResponseEntity<GeneralResponse> deleteNews(@RequestBody NewsRequest inputRequest){
        String deleteNewsName = newsService.deleteNews(inputRequest);
        Body<String> newsBody = new Body<>(deleteNewsName);
        GeneralResponse newsResponse =  GeneralResponse.builder().hdr("Success").bd(newsBody).build();
        System.out.println(newsResponse.toString());
        return ResponseEntity.ok(newsResponse);
    }
}
