package com.example.learnreactjsBE.service;

import com.example.learnreactjsBE.mapping.NewsDTOMapping;
import com.example.learnreactjsBE.model.News;
import com.example.learnreactjsBE.model.dto.NewsDTO;
import com.example.learnreactjsBE.model.request.NewsRequest;
import com.example.learnreactjsBE.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class NewsService {

    @Autowired
    NewsRepository newsRepository;

    @Autowired
    NewsDTOMapping newsDTOMapping;

    public List<NewsDTO> getAll() {
        return newsDTOMapping.newsDTOSMapping(newsRepository.findAll());
    }

    public News insertNews(NewsRequest newsRequest) {
        News news = News.builder().name(newsRequest.getName()).level(newsRequest.getLevel()).build();
        return newsRepository.save(news);
    }

    public News updateNews(NewsRequest newsRequest) {
        Optional<News> newsOptional = newsRepository.findById(UUID.fromString(newsRequest.getId()));
        if (newsOptional.isPresent()) {
            newsOptional.get().setName(newsRequest.getName());
            newsOptional.get().setLevel(newsRequest.getLevel());
            return newsRepository.save(newsOptional.get());
        }
        return null;
    }

    public String deleteNews(NewsRequest newsRequest) {
        Optional<News> newsOptional = newsRepository.findById(UUID.fromString(newsRequest.getId()));
        if (newsOptional.isPresent()) {
            try {
                newsOptional.get().setName(newsRequest.getName());
                newsOptional.get().setLevel(newsRequest.getLevel());
                newsRepository.delete(newsOptional.get());
                return newsRequest.getName();
            } catch (Exception e) {
                throw e;
            }
        }
        return null;
    }
}
