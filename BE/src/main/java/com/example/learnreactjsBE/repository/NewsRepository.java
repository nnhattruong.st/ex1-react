package com.example.learnreactjsBE.repository;

import com.example.learnreactjsBE.model.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.UUID;

@Repository
public interface NewsRepository extends JpaRepository<News, UUID> {
}
