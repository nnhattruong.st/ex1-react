package com.example.learnreactjsBE.model.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
public class GeneralResponse {
    @JsonProperty(value = "hdr")
    String hdr;

    @JsonProperty(value = "bd")
    Body bd;
}
