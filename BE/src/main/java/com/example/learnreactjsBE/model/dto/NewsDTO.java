package com.example.learnreactjsBE.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsDTO {
    @JsonProperty(value = "id")
    private UUID id;

    @JsonProperty(value = "name")
    private String description;

    @JsonProperty(value = "level")
    private int level;
}
