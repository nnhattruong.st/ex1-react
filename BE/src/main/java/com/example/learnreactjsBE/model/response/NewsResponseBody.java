package com.example.learnreactjsBE.model.response;

import com.example.learnreactjsBE.model.dto.NewsDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonSerialize
public class NewsResponseBody extends Body<List<NewsDTO>>{

    public NewsResponseBody(List<NewsDTO> body) {
        super(body);
    }

}
