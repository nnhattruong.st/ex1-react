package com.example.learnreactjsBE.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;

@AllArgsConstructor
@Builder
@JsonSerialize
public class Body<T> {
    @JsonProperty(value = "Items")
    T body;
}
