import React, { Component } from 'react';
import Title from './components/Title';
import Search from './components/Search';
import Sort from './components/Sort';
import Form from './components/Form';
import ListItem from './components/ListItem';
import SweetAlert from 'sweetalert-react';
// import Items from './mockdata/Items';
import './../node_modules/sweetalert/dist/sweetalert.css';
// import { v4 as uuidv4 } from 'uuid';
import { orderBy as orderByld } from 'lodash';
import axios from 'axios';

class App extends Component {    
    constructor(props) {    
        super(props);  
        this.handleSort = this.handleSort.bind(this)
        this.state = {    
            items: [],    
            //show popup and delete product
            showDeleteAlert: false,
            showInsertUpdateAlert: false,
            titleAlert: '',
            contentAlert: "",
            //part 4 -- edit product
            indexEdit: 0,
            idEdit: '',
            nameEdit: '',
            levelEdit: 0,
            showForm: false,
            arrayLevel:[0,1,2],
            valueItem: '',
            levelItem: 0,
            sortType: '',
            sortOrder: '',
            
        }    
    }

    render() {
        return (
            <div className="container">
                {/* show alert when delete record */}
                <SweetAlert
                    show={this.state.showAlert}
                    title= {this.state.titleAlert}
                    text={this.state.contentAlert}
                    showCancelButton
                    onOutsideClick={()  => this.setState({ showAlert: false })}
                    onEscapeKey={()     => this.setState({ showAlert: false })}
                    onCancel={()        => this.setState({ showAlert: false })}
                    onConfirm={()       => this.handleDeleteItem()}
                />

                {/* show alert when insert, update record */}
                <SweetAlert
                    show={this.state.showInsertUpdateAlert}
                    title= {this.state.titleAlert}
                    text={this.state.contentAlert}
                    showCancelButton
                    onOutsideClick={()  => this.setState({ showInsertUpdateAlert: false })}
                    onEscapeKey={()     => this.setState({ showInsertUpdateAlert: false })}
                    onCancel={()        => this.setState({ showInsertUpdateAlert: false })}
                    onConfirm={()       => this.setState({ showInsertUpdateAlert: false })}
                />
                <Title />
                {/* <div className="row">  <button type="button" className="btn btn-info btn-block marginB10" */}
                        {/* onClick={() => this.apiGetNews()}
                     > 
                        Call reload API
                     </button>        
                </div> */}
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <Search handleSearch = {this.handleSearch}/>
                    </div>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <Sort sortType={this.state.sortType}
                            sortOrder={this.state.sortOrder}
                            handleSort={this.handleSort}
                        />
                    </div>
                    <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <button type="button" className="btn btn-info btn-block marginB10" onClick={this.handleShowForm}> { (this.state.showForm) ? 'Close Item' : 'Add Item' }</button>
                    </div>
                </div>
                <div className="row marginB10">
                    <div className="col-md-offset-7 col-md-5">
                        <Form showForm={this.state.showForm}                            
                            arrayLevel={this.state.arrayLevel}
                            handleFormInputChange={this.handleFormInputChange}
                            levelItem={this.state.levelItem}
                            handleFormSelectChange = {this.handleFormSelectChange}
                            handleFormClickCancel={this.handleFormClickCancel}
                            handleFormClickSubmit={this.handleFormClickSubmit}
                            handleSort={this.handleSort}
                        />
                    </div>
                </div>
                <ListItem handleShowAlert={this.handleShowAlert}
                        items = {this.state.items}
                        handleEditItem={this.handleEditItem}
                        valueItem={this.state.valueItem}
                        arrayLevel = {this.state.arrayLevel}
                        handleEditClickSubmit = {this.handleEditClickSubmit}
                        handleShowDeleteAlert = {this.handleShowDeleteAlert}
                />
            </div>
        );
    }

     handleShowAlert = (item) => {
        this.setState({
            showInsertUpdateAlert: true,
            titleAlert: item.name,
            idAlert: item.id
        });
    }    

    //Start delete
    handleShowDeleteAlert = (item) => {
        this.setState({
            showAlert: true,
            titleAlert: "Delete Item",
            contentAlert: "Do you want delete this item?",
            idAlert: item.id
        });
    }

    handleDeleteItem = () => {
        let {idAlert} = this.state;

        this.apiDeleteNews(idAlert,"","");
        setTimeout(() => {
            this.apiGetNews();
        }, 1000);
        this.setState({
            showAlert: false
        });
    }
    //End delete

    //Start edit

    handleEditClickCancel = () => {
        this.setState({
            idEdit: ''
        });
    }

    handleEditClickSubmit = (idEdit,nameEdit,levelEdit) => {   
        console.log("APP.js - handleEditClickSubmit " + idEdit);    
        this.apiUpdateNews(idEdit,nameEdit,levelEdit)
        setTimeout(() => {
            this.apiGetNews();
        }, 1000);
    }
    //End edit
    
    //Start insert
    handleShowForm = () => {
        this.setState({
            showForm: !this.state.showForm
        });
    }

    handleFormInputChange = (value) => {
        this.setState({
            valueItem: value
        });
    }
    
    handleFormSelectChange = (value) => {
        this.setState({
            levelItem: value
        });
    }
   
    handleFormClickCancel = () => {
        this.setState({
            valueItem: '',
            levelItem: 0,
            showForm: false
        });
    }

    handleFormClickSubmit = () => {
        let {valueItem,levelItem} = this.state;
        if(valueItem.trim() !== 0){
            this.apiInsertNews(valueItem,levelItem)
            setTimeout(() => {
                this.apiGetNews();
            }, 1000);
            
        }        
    }
    //End insert

    //Start sort
    handleSort = (sortType,sortOrder) => {
        this.setState({
            sortType: sortType,
            sortOrder: sortOrder
        });
        let {items} = this.state;
        this.setState({
            items: orderByld(items, [sortType],[sortOrder])
        });
    }
    //End sort

    //Start search
    handleSearch = (nameSearch) => {
        console.log("name " + nameSearch);
        
        let items = this.state.items;        

        let itemsFilter = items.filter(element => element.name === nameSearch);
        // itemsFilter.map((item, index) => {console.log("itemsFilter " + item.name + " - " + item.level);});
        // items.map((item, index) => {console.log("items " + item.name + " - " + item.level);});
        if(itemsFilter.length==0){
            this.apiGetNews();
        }

        if(itemsFilter.length>0){
            this.setState({
                items: itemsFilter
            });
        }
        
    }
    //End search
    

    //Load data at first time
    componentDidMount() {        
        this.apiGetNews();
        // console.log("componentDidMount");
    }


    ///////////////////////////////////////////////////////////////// API code//////////////////////////////////////////////
    apiGetNews () {            
        axios({
            method: 'get',
            url: 'http://localhost:8081/news/get-all-news',
          })
        .then(res => {            
            const Items = res.data.bd.Items;   
            // let arrayLevel = [];            
            // if(Items.length > 0) {
            //     for(let i = 0; i < Items.length; i++) {
            //         if(arrayLevel.indexOf(Items[i].level) === -1) {
            //             arrayLevel.push(Items[i].level);
            //         }
            //     }
            // }
            // arrayLevel.sort(function(a, b){return a - b});                 
            // console.log("apiGetNews: " + Items);
            this.setState({
                items : Items,
                // arrayLevel: arrayLevel             
            })
           
        })
        .catch(console.log)
    }

    apiInsertNews = (name, level) => {            
        axios({
            method: 'post',
            url: 'http://localhost:8081/news/insert-news',
            data: {
                id: '',
              name: name,
              level: level
            }
          })
        .then(res => {
            let insertContent = "Insert success record " + res.data.bd.Items.name
            console.log("apiInsertNews: " + res.data.bd.Items.name);
            this.setState({
                showInsertUpdateAlert : true,
                titleAlert: 'Insert Success',
                contentAlert: insertContent,
                showForm: false           
            })
           
        })
        .catch(console.log)
    }

    apiUpdateNews = (id,name, level) => {            
        axios({
            method: 'put',
            url: 'http://localhost:8081/news/update-news',
            data: {
                id: id,
              name: name,
              level: level
            }
          })
        .then(res => {
            let insertContent = "Update success record " + res.data.bd.Items.name
            console.log("apiUpdateNews: " + res.data.bd.Items.name);
            this.setState({
                showInsertUpdateAlert : true,
                titleAlert: 'Update Success',
                contentAlert: insertContent,
                showForm: false           
            })
           
        })
        .catch(console.log)
    }

    apiDeleteNews = (id,name, level) => {            
        axios({
            method: 'delete',
            url: 'http://localhost:8081/news/delete-news',
            data: {
                id: id,
              name: name,
              level: level
            }
          })
        .then(res => {
            let insertContent = "Update success record " + res.data.bd.Items.name
            console.log("apiUpdateNews: " + res.data.bd.Items.name);
            this.setState({
                // showInsertUpdateAlert : true,
                // titleAlert: 'Delete Success',
                // contentAlert: insertContent,
                showAlert: false           
            })
           
        })
        .catch(console.log)
    }
 
}

export default App