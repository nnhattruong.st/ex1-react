import React, {Component} from 'react';
import Item from './Item';
// import Items from '../mockdata/Items';
import ItemEdit from '../components/ItemEdit';
import axios from 'axios';


class ListItem extends Component {
    constructor(props) {    
        super(props);                
        this.state = {        
            items: this.props.items,
            //part 4 -- edit product
            indexEdit: 0,
            idEdit: '',
            nameEdit: '',
            levelEdit: 0,
            arrayLevel: ''            
        }     
    }
    

    render() {
        return(
            <div className="panel panel-success">
                <div className="panel-heading">List Item</div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th style={{ width: '10%' }} className="text-center">#</th>
                            <th>Name</th>
                            <th style={{ width: '15%' }} className="text-center">Level</th>
                            <th style={{ width: '15%' }}>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderItem()}
                    </tbody>
                </table>
            </div>
        )
    }

    renderItem = () => {
        const {idEdit,indexEdit,nameEdit,levelEdit} = this.state;
        let items = this.props.items;
        let arrayLevel = this.props.arrayLevel;
        if(items.length === 0) {
            return <Item item={0} />
        }
        return items.map((item, index) => {
            // console.log("listitem: " + item.id + " " + item.level);
            if(item.id === idEdit) {
                return (
                    <ItemEdit 
                        key={index}
                        indexEdit={indexEdit}
                        nameEdit={nameEdit}
                        levelEdit={levelEdit}
                        arrayLevel={arrayLevel}
                        handleEditClickCancel={this.handleEditClickCancel}
                        handleEditInputChange={this.handleEditInputChange}
                        handleEditSelectChange={this.handleEditSelectChange}    
                        handleEditClickSubmit={this.handleEditClickSubmit}
                
                    />
                )
            }
            return (
                <Item
                    key={index}
                    item={item}
                    index={index}
                    arrayLevel={item.arrayLevel}
                    handleShowAlert={this.handleShowAlert}       
                    handleEditItem={this.handleEditItem}             
                    handleShowDeleteAlert={this.handleShowDeleteAlert}
                  />
            )
        });
    }

    handleShowDeleteAlert = (item) => {
        this.props.handleShowDeleteAlert(item)
        // console.log(this.props.items);
    }

    handleShowAlert = (item) => {
        this.props.handleShowAlert(item)
        // console.log(this.props.items);
    }

    handleEditItem = (index,item) => {
        console.log("handleEditItem " + item.id);
        this.setState({
            indexEdit: index,
            idEdit: item.id,
            nameEdit: item.name,
            levelEdit: item.level
        });
    }

    handleEditClickCancel = () => {
        this.setState({
            idEdit: ''
        });
    }

    handleEditInputChange = (value) => {
        this.setState({
            nameEdit: value
        });
    }

    handleEditSelectChange = (value) => {
        this.setState({
            levelEdit: value
        });
    }

    handleEditClickSubmit = () => {
        let {items,idEdit,nameEdit,levelEdit} = this.state;
        // console.log("handleEditClickSubmit" + idEdit);
        this.props.handleEditClickSubmit(idEdit,nameEdit,levelEdit);       
        this.setState({
            idEdit: '',
            items: items
        }); 
    }

    // apiUpdateNews = (id,name, level) => {            
    //     axios({
    //         method: 'post',
    //         url: 'http://localhost:8081/news/update-news',
    //         data: {
    //             id: id,
    //           name: name,
    //           level: level
    //         }
    //       })
    //     .then(res => {
    //         let insertContent = "Update success record " + res.data.bd.Items.name
    //         // console.log("apiUpdateNews: " + res.data.bd.Items.name);
    //         this.setState({
    //             showInsertUpdateAlert : true,
    //             titleAlert: 'Update Success',
    //             contentAlert: insertContent,
    //             showForm: false           
    //         })
           
    //     })
    //     .catch(console.log)
    // }

    // //Load data at first time
    // componentDidMount() {        
    //     this.apiGetNews();
    //     // console.log("componentDidMount");
    // }


    // ///////////////////////////////////////////////////////////////// API code//////////////////////////////////////////////
    // apiGetNews () {            
    //     axios({
    //         method: 'get',
    //         url: 'http://localhost:8081/news/get-all-news',
    //       })
    //     .then(res => {            
    //         const Items = res.data.bd.Items;   
    //         let arrayLevel = [];            
    //         if(Items.length > 0) {
    //             for(let i = 0; i < Items.length; i++) {
    //                 if(arrayLevel.indexOf(Items[i].level) === -1) {
    //                     arrayLevel.push(Items[i].level);
    //                 }
    //             }
    //         }
    //         arrayLevel.sort(function(a, b){return a - b});                 
    //         console.log("apiGetNews: " + Items);
    //         this.setState({
    //             items : Items,
    //             arrayLevel: arrayLevel             
    //         })
           
    //     })
    //     .catch(console.log)
    // }
}
      export default ListItem;    