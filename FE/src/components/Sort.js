import React, {Component} from 'react';

class Sort extends Component {

    constructor(props) {    
        super(props);    
        this.state = {
            display: 'none'             
        }
    }
    handleClick = (sortType,sortOrder) => {
        this.props.handleSort(sortType,sortOrder);        
    }
    
    renderSort = () => {
        let {sortType, sortOrder} = this.props;
        if(sortType !== '' && sortOrder !== '') {
            return (
                <span className="label label-success label-medium text-uppercase">
                    {/* sadfsdfasdf - 12312313 */}
                    {sortType} - {sortOrder}
                </span>
            )
        }
    }

    // renderDropdownMenu = () => {
    //     let {sortType, sortOrder} = this.props;
    //     if(this.state.display === 'true') {
    //         return (
    //             <ul className="dropdown-menu-true" id="dropdownMenu1">
    //                 <li onClick={() => this.handleClick('name','asc')}>
    //                     <a role="button" className="text-uppercase">Name ASC</a>
    //                 </li>
    //                 <li onClick={() => this.handleClick('name','desc')}>
    //                     <a role="button" className="text-uppercase">Name DESC</a>
    //                 </li>
    //                 <li role="separator" className="divider"></li>
    //                 <li onClick={() => this.handleClick('level','asc')}>
    //                     <a role="button" className="text-uppercase">Level ASC</a>
    //                 </li>
    //                 <li onClick={() => this.handleClick('level','desc')}>
    //                     <a role="button" className="text-uppercase">Level DESC</a>
    //                 </li>
    //             </ul>
    //         )
    //     }
    // }

    showDropdown = () => {
        this.setState({
            display: 'inline'
        });
    }
    hideDropdown = () => {
        this.setState({
            display: 'none'
        });
    }

    render() {
        return(
            <div className="dropdown">                
                <button className="btn btn-default dropdown-toggle" type="button" 
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" onClick = {() => this.showDropdown()}>
                    Sort by <span className="caret" />
                </button>
                <ul className="dropdown-menu" style={{display:this.state.display}} id="dropdownMenu1">
                    <li onClick={() => {this.handleClick('name','asc'); this.hideDropdown();}}>
                        <a role="button" className="text-uppercase">Name ASC</a>
                    </li>
                    <li onClick={() => {this.handleClick('name','desc'); this.hideDropdown();}}>
                        <a role="button" className="text-uppercase">Name DESC</a>
                    </li>
                    <li role="separator" className="divider"></li>
                    <li onClick={() => {this.handleClick('level','asc'); this.hideDropdown();}}>
                        <a role="button" className="text-uppercase">Level ASC</a>
                    </li>
                    <li onClick={() => {this.handleClick('level','desc'); this.hideDropdown();}}>
                        <a role="button" className="text-uppercase">Level DESC</a>
                    </li>
                </ul>
                {this.renderSort()}
            </div>
        )
    }

    handleClick = (sortType,sortOrder) => {
        this.props.handleSort(sortType,sortOrder);
    }
}

export default Sort;