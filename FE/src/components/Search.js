import React, {Component} from 'react';

class Search extends Component {
    constructor(props) {    
        super(props);      
    }

    render() {
        return(
            <div className="input-group">
                <input id="inputSearch" type="text" className="form-control" placeholder="Search item name" onChange = {(event)=>{this.props.handleSearch(event.target.value)}} 
                />
                <span className="input-group-btn">
                    <button className="btn btn-info" type="button" 
                    onClick={()=>{                        
                            // this.setState({clearState: 'true'})      
                            document.getElementById('inputSearch').value = "";                
                        }
                    }
                    >Clear</button>
                </span>
            </div>
        )
    }

    // renderInput = () => {
    //     if(this.state.clearState === 'false'){
    //         return(
    //             <input type="text" className="form-control" placeholder="Search item name" onChange = {(event)=>this.props.handleSearch(event.target.value)}/>
    //         )
    //     }
    //     return(
    //         <input type="text" className="form-control" placeholder="Search item name" />
    //     )
    // }

    // clearInput = () => {
    //     this.setState({clearState: 'true'});
    // }

    // handleEditItem = (index,item) => {
    //     this.setState({
    //         indexEdit: index,
    //         idEdit: item.id,
    //         nameEdit: item.name,
    //         levelEdit: item.level
    //     });
    // }

}

export default Search;